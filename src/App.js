import { useState } from 'react';
import Navbar from './assets/components/Navbar';
import { Stack, TextField } from '@mui/material';
import UserData from './assets/components/UserData';
import UserDialog from './assets/components/UserDialog';

function App() {

  const [users, setUsers] = useState([]);
  const [open, setOpen] = useState(false);
  const [editOpen, setEditOpen] = useState(false);

  const [id, setId] = useState(0);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");

  const [editId, setIdEdit] = useState(-1);
  const [editName, setEditName] = useState("");
  const [editAddress, setEditAddress] = useState("");
  const [editHobby, setEditHobby] = useState("");

  // untuk search
  const [keyword, setKeyword] = useState("");

  const handleOpen = () => {
    setOpen(true);
    clearFields();
  }

  const handleClose = () => {
    setOpen(false);
  }

  const clearFields = () => {
    setName("");
    setAddress("");
    setHobby("");
    setEditName("");
    setEditAddress("");
    setEditHobby("");
  }

  const handleSubmit = () => {
    setId(id + 1);
    setUsers([...users, { id, name, address, hobby }]);
    clearFields();
    handleClose();
  }

  const handleEditOpen = (id) => {
    let user = users.filter(u => u.id === id)[0];
    setIdEdit(id);
    setEditName(user.name);
    setEditAddress(user.address);
    setEditHobby(user.hobby);
    setEditOpen(true);
  }

  const handleEditSubmit = () => {
    let newArr = users;
    let idx = newArr.map(obj => obj.id).indexOf(editId);
    console.log(idx);
    newArr[idx] = {
      id: id,
      name: editName,
      address: editAddress,
      hobby: editHobby
    };

    setUsers(newArr);
    clearFields();
    setEditOpen(false);
  }

  return (
    <>
      <Navbar onOpen={handleOpen} />

      <div className='w-full shadow-md h-24 flex items-center px-3 py-2'>
        <TextField className='w-80' value={keyword} onChange={e => setKeyword(e.target.value)} label={"Search"} />
      </div>

      {/* Add Dialog */}
      <UserDialog
        isOpen={open}
        onClose={handleClose}
        onSubmit={handleSubmit}
      >
        <TextField size='medium' margin='dense' label={"Nama"} value={name} onChange={e => setName(e.target.value)} className='m-2 w-80' />
        <TextField margin='dense' label={"Address"} value={address} onChange={e => setAddress(e.target.value)} className='m-2 w-80' />
        <TextField margin='dense' label={"Hobby"} value={hobby} onChange={e => setHobby(e.target.value)} className='m-2 w-80' />
      </UserDialog>


      {/* Edit Dialog */}
      <UserDialog
        isOpen={editOpen}
        onClose={() => setEditOpen(false)}
        onSubmit={handleEditSubmit}
      >
        <TextField size='medium' margin='dense' label={"Nama"} value={editName} onChange={e => setEditName(e.target.value)} className='m-2 w-80' />
        <TextField margin='dense' label={"Address"} value={editAddress} onChange={e => setEditAddress(e.target.value)} className='m-2 w-80' />
        <TextField margin='dense' label={"Hobby"} value={editHobby} onChange={e => setEditHobby(e.target.value)} className='m-2 w-80' />
      </UserDialog>


      <Stack
        className={`w-full`}
        gap={3}
      >
        {users.filter(u => u.name.toLowerCase().match(keyword.toLowerCase())).map((user, index) => (
          <UserData key={index} onEdit={handleEditOpen} id={user.id} name={user.name} address={user.address} hobby={user.hobby} />
        ))}
      </Stack>
    </>
  );
}

export default App;
