import { Button } from "@mui/material";
import React from "react";

const UserData = ({ id, name, address, hobby, onEdit }) => {
  return (
    <div className="w-full flex justify-between px-3 py-2 border shadow-md shadow-black">
      <div className="flex flex-col w-1/2 items-start gap-3">
        <span className="text-3xl">{name}</span>
        <span className="text-lg">{address}</span>
      </div>
      <div className="flex flex-col w-1/2 justify-center items-center gap-3">
        <span className="text-3xl text-green-600">{hobby}</span>
        <Button
          variant="contained"
          onClick={() => onEdit(id)}
          sx={{
            backgroundColor: "#00C2CB",
            borderRadius: "2rem",
            width: "5rem",
          }}
        >
          Edit
        </Button>
      </div>
    </div>
  );
};

export default UserData;
