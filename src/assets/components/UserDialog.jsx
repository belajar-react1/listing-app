import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Stack,
} from "@mui/material";
import React from "react";

const UserDialog = ({ isOpen, onClose, onSubmit, children }) => {
  return (
    <Dialog maxWidth={"xl"} open={isOpen} onClose={onClose}>
      <DialogTitle>{"Tambah User"}</DialogTitle>
      <DialogContent className="py-5 px-3">
        <Stack gap={3}>{children}</Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={onSubmit}>Submit</Button>
      </DialogActions>
    </Dialog>
  );
};

export default UserDialog;
